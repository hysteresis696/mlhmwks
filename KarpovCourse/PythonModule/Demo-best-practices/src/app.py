from fastapi import FastAPI, Depends
import psycopg2
import yaml
import uvicorn
from psycopg2.extras import RealDictCursor
from psycopg2.extensions import connection
import os
from dotenv import load_dotenv

from pathlib import Path
import sys

sys.path.append(str(Path(__file__).parent.parent))

from src.crud import get_feed, get_likes

app = FastAPI()


def get_db():
    with psycopg2.connect(
            user=os.environ.get('POSTGRES_USER'),
            password=os.environ.get('POSTGRES_PASSWORD'),
            host=os.environ.get('POSTGRES_HOST'),
            port=os.environ.get('POSTGRES_PORT'),
            database=os.environ.get('POSTGRES_DATABASE'),
    ) as conn:
        return conn


def config():
    print(__file__)
    with open(Path(__file__).parent.parent / "params.yaml", "r") as f:
        return yaml.safe_load(f)


@app.get("/user")
def get_all_users(limit: int = 10, db=Depends(get_db)):
    with db.cursor(cursor_factory=RealDictCursor) as cursor:  # type: cursor
        cursor.execute(
            """
            SELECT *
            FROM "user"
            LIMIT %(limit)s
            """,
            {
                "limit": limit
            }
        )
        return cursor.fetchall()


@app.get("user/feed")
def get_user_feed(user_id: int, limit: int = 10, conn: connection = Depends(get_db), config: dict = Depends(config)):
    return get_feed(conn, user_id, limit, config)


@app.get("/user/likes")
def get_user_likes(user_id: int, limit: int = 10, conn: connection = Depends(get_db), config: dict = Depends(config)):
    return get_likes(conn, user_id, limit, config)


if __name__ == '__main__':
    load_dotenv()
    uvicorn.run(app)
