"""Продолжаем улучшать вектор. Добавьте в класс возможность умножать вектор на вектор и вектор на число. Не забудьте
сохранять координаты вектора в self.coords. Vector([1, 2, 3]) * Vector([2, 5, -2])  # даст 6 # 1 * 2 + 2 * 5 + 3 * (
-2) = 6

Vector([1, 2]) * Vector([2, 3, 4])
# ValueError: left and right lengths differ: 2 != 3

Vector([2, 3, 5, 8]) * 5  # даст Vector([10, 15, 25, 40])
"""


class Vector:

    def __init__(self, coords_list: list):
        self.coords = coords_list

    def __add__(self, other):
        if len(self.coords) != len(other.coords):
            raise ValueError(f"left and right lengths differ: {len(self.coords)} != {len(other.coords)}")
        result = [self.coords[i] + other.coords[i] for i in range(len(other.coords))]
        return result

    def __str__(self):
        return f'{self.coords}'

    def __mul__(self, other):
        if isinstance(other, Vector):
            if len(self.coords) != len(other.coords):
                raise ValueError(f'left and right lengths differ: {len(self.coords)} != {len(other.coords)}')
            return sum([self.coords[i] * other.coords[i] for i in range(len(self.coords))])
        return [self.coords[i] * other for i in range(len(self.coords))]

