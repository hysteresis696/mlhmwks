"""Добавим могущества нашему вектору.
Добавьте методу красивый вывод при передаче его в print(...). Пример:
print(Vector([1, 2, 3]))
# Напечатает: "[1, 2, 3]" без кавычек

vec = Vector([1])
print(vec)
# Напечатает "[1]" без кавычек
"""


class Vector:

    def __init__(self, cords_list: list):
        self.cords = cords_list

    def __add__(self, other):
        if len(self.cords) != len(other.coords):
            raise ValueError(f"left and right lengths differ: {len(self.cords)} != {len(other.coords)}")
        result = [self.cords[i] + other.coords[i] for i in range(len(other.coords))]
        return result

    def __str__(self):
        return f'{self.cords}'
