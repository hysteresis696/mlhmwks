"""Используя миксины из прошлого пункта, напишите класс JsonHandler, который будет наследоваться от классов ParsesBody
и ParsesHeaders , иметь метод process() и конструктор, принимающий аргумент request и сохраняющий в self.request.
 В этом задании нужно использовать библиотеку json.
Метод process() должен работать следующим образом:
1.	Если need_json() дает False, то возвращать None
2.	Иначе получать тело через body(), пытаться считать его как json.loads(...) и возвращать число ключей в словаре.
 Если считать не удалось, то вернуть None.
Отправьте реализацию класса JsonHandler в LMS. Классы ParsesBody и ParsesHeaders отправлять не нужно.
Обратите внимание, что с помощью миксин функциональность проверки headers и получения body была вынесена за JsonHandler
 - наш класс сосредоточился именно на обработке.
 # Пример использования
r = {'body': '{"a": 123, "b": 1234}',
     'headers': {'content-type': 'application/json'}
    }
print(JsonHandler(r).process())

"""

from MultipleInheritance1 import ParsesBody, ParsesHeaders
import json


class JsonHandler(ParsesBody, ParsesHeaders):

    def __init__(self, request):
        self.request = request

    def process(self):
        if super().need_json():
            try:
                return len(json.loads(super().body()))
            except json.decoder.JSONDecodeError:
                return None
        return None
