"""Напишите класс Vector, который на вход будет принимать список координат (x1,…, xn). Положите все координаты вектора
в список self.coords. Добейтесь того, чтобы объекты класса Vector можно было складывать через оператор + и получать
на выходе тоже объект этого же класса. Например: # Будет работать Vector([1, 2, 3]) + Vector((2, 3, 4)) # даст
Vector([3, 5, 7])

# НЕ будет работать
Vector([1, 2]) + Vector([1, 2, 3]) # нельзя складывать векторы разной длины
# Должно возвращать ошибку (сообщение тоже!)
# ValueError: left and right lengths differ: 2 != 3
"""


class Vector:

    def __init__(self, coords_list: list):
        self.coords = coords_list

    def __add__(self, other):
        if len(self.coords) != len(other.coords):
            raise ValueError(f"left and right lengths differ: {len(self.coords)} != {len(other.coords)}")
        result = [self.coords[i] + other.coords[i] for i in range(len(other.coords))]
        return result
