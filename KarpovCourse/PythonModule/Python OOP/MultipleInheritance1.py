"""Попробуем сделать множественное наследование аккуратно и так, как его обычно делают в популярных библиотеках.
Обычно множественное наследование используют в т.н. Mixins. Mixin (рус. миксины) - это класс, не имеющий полей и
имеющий один или более не абстрактных методов (т.е. методов с реализацией). Обычно миксины используют для того,
чтобы модульно раздавать различные функциональности различным объектам. В лекции был пример с Connectable и
PostgresqlConnection. Connectable как раз был одной из Mixin. Вас перевели из тех.директора сложного проекта в
бекенд-разработчика. Встала задача разработать класс, которому на вход будет приходить разобранный запрос от
пользователя. Запрос имеет вид:

 # будет приходить запрос в виде словаря
 request = {
  "cookies": {key_1: value_1, key_2: value_2, ...},
  "body": "a long time ago, in a Galaxy far, far away",
  "headers": {"content-type": "application/json", "Accept": "application/json"}
}
# и этот словарь будет передаваться в конструктор класса handler = Handler(request)
Разным классам в приложении потребуется разная функциональность: кому-то потребуется проверять,
есть ли в headers ключ "Accept", кому-то потребуется читать body, а кому-то понадобится проверять пустоту cookies.
Будут и классы, которым потребуется несколько возможностей сразу. Напишите классы ParsesCookies, ParsesBody,
ParsesHeaders по условиям:
Напишите классы ParsesCookies, ParsesBody, ParsesHeaders по условиям:

Класс ParsesCookies имеет метод cookies(), возвращающий все по ключу cookies из словаря self.request.
Класс ParsesCookies имеет метод is_authed(), который будет проверять, что в словаре cookies будет ключ auth_key
(ни в коем случае не используйте такую авторизацию в реальных проектах).
Класс ParsesBody имеет метод body(), возвращающий текст по ключу body в self.request.
Класс ParsesHeaders имеет метод headers(), возвращающий все по ключу headers из словаря self.request.
Класс ParsesHeaders имеет метод need_json(), который возвращает True, если в headers по ключу "content-type" лежит значение "application/json", иначе False."""


class ParsesCookies:

    def cookies(self):
        return self.request['cookies']

    def is_authed(self):
        return 'auth_key' in self.request['cookies']


class ParsesBody:

    def body(self):
        return self.request['body']


class ParsesHeaders:

    def headers(self):
        return self.request['headers']

    def need_json(self):
        return self.request.get('headers').get('content-type') == 'application/json'