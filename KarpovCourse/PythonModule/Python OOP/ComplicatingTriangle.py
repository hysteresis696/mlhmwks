"""Усложняем треугольник Возьмите класс Triangle из предыдущего задания и добавьте метод area(), возвращающий площадь
треугольника. Напомним, что при известных трех сторонах площадь треугольника можно подсчитать по формуле Герона: S=p(
p−a)(p−b)(p−c), где p=21(a+b+c) - полупериметр. Подумайте, как можно организовать код так, чтобы p считалась один
раз. Затем поменяйте конструктор: он должен проверять, что выполнено неравенство треугольника - каждая сторона меньше
или равна сумме двух других. Если это условие не выполнено, выбрасывайте ValueError с текстом "triangle inequality
does not hold" (передайте эту строку в конструктор ValueError). Наконец, создайте два объекта данного класса с
названиями tr_1 и tr_2 , в которых соблюдается равенство треугольника. Также, сохраните в переменные square_1 и
square_2 результаты вызовов методов .area() для объектов tr_1 и tr_2 соответственно."""


class Triangle:
    n_dots = 3

    def __init__(self, a, b, c):
        self.a, self.b, self.c = a, b, c
        self.p = 0.5 * (a + b + c)
        if (self.a <= self.b + self.c) and (self.b <= self.a + self.c) and (self.c <= self.a + self.b):
            pass
        else:
            raise ValueError("triangle inequality does not hold")

    def area(self):
        return self.p * (self.p - self.a) * (self.p - self.b) * (self.p - self.c)


tr_1 = Triangle(3, 4, 5)
square_1 = tr_1.area()

tr_2 = Triangle(14, 16, 10)
square_2 = tr_2.area()

print(square_1)
print(square_2)
