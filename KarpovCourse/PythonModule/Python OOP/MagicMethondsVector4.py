"""Последние штрихи для нашего вектора.
Добавьте в класс возможности сравнивать два вектора между собой и считать abs (это длина вектора, в Евклидовой метрике).
abs(Vector([-12, 5]))  # Должно быть 13

Vector([1, 3, 5]) == Vector([1])  # False Vector([1, 3, 5]) == Vector([-1, 3, 5])  # False Vector([1, 3,
5]) == Vector([1, 3, 5])  # True По итогу мы получим вектора, которые можно складывать, умножать, печатать,
считать длину и сравнивать на равенство друг с другом."""


class Vector:

    def __init__(self, cords_list: list):
        self.coords = cords_list

    def __add__(self, other):
        if len(self.coords) != len(other.coords):
            raise ValueError(f"left and right lengths differ: {len(self.coords)} != {len(other.coords)}")
        result = [self.coords[i] + other.coords[i] for i in range(len(other.coords))]
        return result

    def __str__(self):
        return f'{self.coords}'

    def __mul__(self, other):
        if isinstance(other, Vector):
            if len(self.coords) != len(other.coords):
                raise ValueError(f'left and right lengths differ: {len(self.coords)} != {len(other.coords)}')
            return sum([self.coords[i] * other.coords[i] for i in range(len(self.coords))])
        return [self.coords[i] * other for i in range(len(self.coords))]

    def __eq__(self, other):
        if isinstance(other, Vector) and (self.coords == other.coords):
            return True
        return False

    def __abs__(self):
        return sum([x ** 2 for x in self.coords]) ** 0.5
