"""Наследование: абстрактный класс (3/4) Возьмите классы Triangle и Rectangle из прошлого задания. 1.	Переопределите
метод area в каждом случае. 2.	Переопределите конструктор в каждом случае (число аргументов тоже меняется). Не
забудьте в конструкторе дочернего класса вызвать конструктор родительского класса! 3.	Переопределите метод validate
в каждом случае. Метод validate должен принимать только аргумент self и использовать созданные в конструкторе
переменные. Для этого вы можете сначала сохранять в конструкторе входные данные в self.переменная, а затем вызывать
конструктор суперкласса. Для Triangle данный метод должен проверять неравенство треугольника и выбрасывать ошибку
ValueError("triangle inequality does not hold") либо возвращать значения сторон. Для Rectangle данный метод должен
возвращать значения сторон. В итоге вы получите два класса, которые построены по схожему шаблону. Этот общий шаблон
был задан в классе BaseFigure. Создайте несколько объектов этих классов и попробуйте вызвать у них .area(),
.validate(). Если вы пользуетесь IDE, то увидите интерактивные подсказки: она скажет, что такие методы есть и что эти
методы перегружают (overload) методы из родительского класса. При этом вызов методов будет работать коррректно."""

from InheritanceAbstractClass1 import BaseFigure


class Triamgle(BaseFigure):
    n_dots = 3

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.p = 0.5 * (a * b * c)
        super().__init__()

    def validate(self):
        if (self.a <= self.b + self.c) \
                and (self.b <= self.a + self.c) \
                and (self.c <= self.a + self.b):
            return self.a, self.b, self.c
        raise ValueError("triangle inequality does not hold")

    def area(self):
        return (self.p * (self.p - self.a)
                * (self.p - self.b)
                * (self.p - self.c))


class Rectangle(BaseFigure):
    n_dots = 4

    def __init__(self, a, b):
        self.a = a
        self.b = b
        super().__init__()

    def validate(self):
        if self.a == self.b:
            return self.a, self.b
        else:
            raise ValueError("rectangle inequality does not hold")

    def area(self):
        return self.a * self.b
