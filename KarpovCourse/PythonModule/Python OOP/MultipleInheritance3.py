"""
Используя миксины из прошлого пункта, напишите класс SecureTextHandler, который будет наследоваться от классов ParsesBody,
 ParsesHeaders и ParsesCookies , иметь метод process() и конструктор, принимающий один аргумент и сохраняющий
  его в нужное поле класса.
Метод process() должен работать следующим образом:
1.	Если is_authed() дает False, то возвращать None.
2.	Иначе получать тело через body() и возвращать его длину.
Добейтесь работоспособности на примере и реализацию класса SecureTextHandler отправьте в LMS. Классы ParsesBody
 и ParsesHeaders, ParsesCookie отправлять не нужно.

# Примеры
r = {'cookies': {'auth_key': '123'},
     'body': 'hello'
    }
print(SecureTextHandler(r).process())
# 5

r = {'cookies': {},
     'body': 'hello'
    }
print(SecureTextHandler(r).process())
# None
"""
from MultipleInheritance1 import *


class SecureTextHandler(ParsesHeaders, ParsesBody, ParsesCookies):

    def __init__(self, request):
        self.request = request

    def process(self):
        if super().is_authed():
            return len(super().body())
        return None

