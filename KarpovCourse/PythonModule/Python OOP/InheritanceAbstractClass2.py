"""Наследование: абстрактный класс (2/4) Перепишите классы Triangle, Rectangle так, чтобы они наследовались от
BaseFigure. Затем уберите реализацию всех методов и конструкторов в классах-потомках. Есть ли у Triangle,
Rectangle методы area, validate? Если есть, то что они возвращают при вызове?"""

from InheritanceAbstractClass1 import BaseFigure


class Triangle(BaseFigure):
    n_dots = 3


class Rectangle(BaseFigure):
    n_dots = 4


rec = Rectangle()

rec.validate()

#Ответ: Методы есть ты они выдают ошибку которая raise'нута в BaseFigure