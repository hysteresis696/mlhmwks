"""Простое наследование Создайте класс Rectangle (прямоугольник), который будет наследоваться от класса Triangle.
Сделайте так, чтобы area(), конструктор и поле n_dots были верными. А именно: 1.	Конструктор должен принимать 2
стороны: a, b 2.	area() должен считать площадь как произведение смежных сторон: S=ab 3.	Неравенство треугольника
не нужно проверять. 4.	n_dots должен быть объявлен на уровне класса и равняться 4. Отправьте реализацию класса
Rectangle в LMS. Класс Triangle отправлять не нужно."""


class Triangle:
    n_dots = 3

    def __init__(self, a, b, c):
        self.a, self.b, self.c = a, b, c
        self.p = 0.5 * (a + b + c)
        if (self.a <= self.b + self.c) and (self.b <= self.a + self.c) and (self.c <= self.a + self.b):
            pass
        else:
            raise ValueError("triangle inequality does not hold")

    def area(self):
        return self.p * (self.p - self.a) * (self.p - self.b) * (self.p - self.c)


class Rectangle(Triangle):
    n_dots = 4

    def __init__(self, a, b):
        self.a = a
        self.b = b
        if self.a == self.b:
            pass
        else:
            raise ValueError("rectangle inequality does not hold")

    def area(self):
        return self.a * self.b
