"""
A DAG with dynamic tasks first 10 with bash and another with PythonOperator
"""
from datetime import timedelta, datetime
from textwrap import dedent
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator

with DAG(
        'hmwk_2_DAG',
        default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5),
        },
        description='A cycle generator DAG',
        schedule_interval=timedelta(days=1),
        start_date=datetime(2024, 3, 20),
        catchup=False,
        tags=['CycleDagTag']
) as dag:

    dag.doc_md = __doc__

    for numbers in range(10):
        bash_task = BashOperator(
            task_id=f'print_{numbers}_in_the_terminal',
            bash_command=f"echo {numbers}"
        )

    bash_task.doc_md = dedent(
        """
        return number of task from 0 to 9
        """
    )


    def print_task_number(task_number):
        print(f"task number is {task_number}")


    for numbers in range(20):
        python_task = PythonOperator(
            task_id=f"tsk_number:{numbers}",
            python_callable=print_task_number,
            op_kwargs={"task_number": numbers}
        )

    python_task.doc_md = dedent(
        """
        return number of task from 0 to 19
        """
    )

    bash_task >> python_task
