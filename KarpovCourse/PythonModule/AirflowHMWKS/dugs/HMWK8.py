"""
A DAG with Xcom
"""
from datetime import timedelta, datetime
# from textwrap import dedent
from airflow import DAG
# from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator

with DAG(
        'hw',
        # Параметры по умолчанию для тасок
        default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5),  # timedelta из пакета datetime
        },
        # Описание DAG (не тасок, а самого DAG)
        description='A cycle generated DAG of homework of Lesson 11',
        # Как часто запускать DAG
        schedule_interval=timedelta(days=1),
        # С какой даты начать запускать DAG
        # Каждый DAG "видит" свою "дату запуска"
        # это когда он предположительно должен был
        # запуститься. Не всегда совпадает с датой на вашем компьютере
        start_date=datetime(2022, 4, 1),
        # Запустить за старые даты относительно сегодня
        # https://airflow.apache.org/docs/apache-airflow/stable/dag-run.html
        catchup=False,
        # теги, способ помечать даги
        tags=['HW_8']
) as dag:

    dag.doc_md = __doc__

    def set_xcom_val(ti):
        ti.xcom_push(
            key="sample_xcom_key",
            value="xcom text"
        )

    def get_xcom_text(ti):
        result = ti.xcom_pull(key="sample_xcom_key", task_ids='set_xcom_val')
        print(result)

    t1 = PythonOperator(
        task_id='set_xcom_val',
        python_callable=set_xcom_val,
    )

    t2 = PythonOperator(
        task_id='get_xcom_val',
        python_callable=get_xcom_text
    )

    t1 >> t2