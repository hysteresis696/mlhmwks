"""
This is dag from first hmwk of lesson 11 and here is 2 tasks
and 2 operators(BashOperator and PythonOperator)
"""
from datetime import timedelta, datetime
from textwrap import dedent

from airflow import DAG

from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator

with DAG(
        'hmwk1',
        default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5)
        },
        description='A simple DAG from Lesson11 hmwk',
        schedule_interval=timedelta(days=1),
        start_date=datetime(2024, 3, 20),
        catchup=False,
        tags=['example DAG']
) as dag:
    date = "{{ ds }}"

    t1 = BashOperator(
        task_id="print_pwd",
        bash_command="pwd"
    )

    t1.doc_md = dedent(
        """
        Print working directory as bash's command `pwd`
        """
    )

    dag.doc_md = __doc__


    def print_ds(ds):
        print(ds)


    t2 = PythonOperator(
        task_id='print_ds',
        python_callable=print_ds,
    )

    t2.doc_md = dedent(
        """
        PythonOperator get logical date(ds) and print it 
        """
    )

    t1 >> t2
