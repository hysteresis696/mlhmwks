from datetime import timedelta, datetime
from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator


default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5),  # timedelta из пакета datetime
        }

with DAG(
        'hw_12',
        # Параметры по умолчанию для тасок
        default_args=default_args,
    # Описание DAG (не тасок, а самого DAG)
    description='A cycle generated DAG of homework of Lesson 11',
    # Как часто запускать DAG
    schedule_interval=timedelta(days=1),
    # С какой даты начать запускать DAG
    # Каждый DAG "видит" свою "дату запуска"
    # это когда он предположительно должен был
    # запуститься. Не всегда совпадает с датой на вашем компьютере
    start_date=datetime(2022, 4, 1),
    # Запустить за старые даты относительно сегодня
    # https://airflow.apache.org/docs/apache-airflow/stable/dag-run.html
    catchup=False,
    # теги, способ помечать даги
    tags=['HW_12']
) as dag:

    def get_is_startml():
        from airflow.models import Variable
        return Variable.get("is_startml")

    def is_startml_task():
        print("StartML is a starter course for ambitious people")

    def is_not_startml_task():
        print("Not a startML course, sorry")

    def get_task():
        is_startml = get_is_startml
        if is_startml == True:
            return "startml_desc"
        else:
            return "not_startml_desc"

    branc_op = BranchPythonOperator(
        task_id='select branch',
        python_callable= get_task
    )

    start_op = PythonOperator(
        task_id='startml_desc',
        python_callable=is_startml_task
    )

    not_start_op = PythonOperator(
        task_id='not_startml_desc',
        python_callable=is_not_startml_task
    )

    branc_op >> [start_op, not_start_op]
