"""
A DAG with one task with pattern
"""

from datetime import timedelta, datetime
from textwrap import dedent
from airflow import DAG
from airflow.operators.bash import BashOperator

with DAG(
    'hmwk4',
    default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5),
    },
    description='A DAG with pattern',
    start_date=datetime(2023, 3, 20),
    schedule_interval=timedelta(days=1),
    catchup=False,
    tags=['hmwk_4']
) as dag:
    template_command = dedent(
        """
        {% for i in range(5)%}
            echo "{{ ts }}"
            echo "{{ run_id }}"
        {% endfor %}
        """
    )

    bash_task = BashOperator(
        task_id='templated',
        bash_command=template_command
    )

    dag.doc_md = __doc__

    bash_task.doc_md = dedent(
        """
        return templated variables
        """
    )

    bash_task
