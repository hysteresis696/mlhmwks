"""
A DAG with 2 XCom and 2 PythonOperator
"""
from datetime import timedelta, datetime
from airflow import DAG
from airflow.operators.python import PythonOperator

with DAG(
        'hw_6_a.kosharov',
        # Параметры по умолчанию для тасок
        default_args={
            'depends_on_past': False,
            'email': ['airflow@example.com'],
            'email_on_failure': False,
            'email_on_retry': False,
            'retries': 1,
            'retry_delay': timedelta(minutes=5),  # timedelta из пакета datetime
        },
        # Описание DAG (не тасок, а самого DAG)
        description='A cycle generated DAG of homework of Lesson 11',
        # Как часто запускать DAG
        schedule_interval=timedelta(days=1),
        # С какой даты начать запускать DAG
        # Каждый DAG "видит" свою "дату запуска"
        # это когда он предположительно должен был
        # запуститься. Не всегда совпадает с датой на вашем компьютере
        start_date=datetime(2022, 4, 1),
        # Запустить за старые даты относительно сегодня
        # https://airflow.apache.org/docs/apache-airflow/stable/dag-run.html
        catchup=False,
        # теги, способ помечать даги
        tags=['HW_8']
) as dag:

    dag.doc_md = __doc__

    def save_xcom_text():
        return "Airflow tracks everything"

    def get_xcom_text(ti):
        result = ti.xcom_pull(key="return_value", task_ids="save_text_xcom")
        print(result)

    t1 = PythonOperator(
        task_id="save_text_xcom",
        python_callable=save_xcom_text
   )

    t2 = PythonOperator(
        task_id="get_xcom_text",
        python_callable=get_xcom_text
    )

    t1 >> t2

