from datetime import timedelta, datetime
from airflow import DAG
from airflow.operators.python import PythonOperator

default_args = {
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),  # timedelta из пакета datetime
}

with DAG(
        'hw_11',
        # Параметры по умолчанию для тасок
        default_args=default_args,
        # Описание DAG (не тасок, а самого DAG)
        description='A cycle generated DAG of homework of Lesson 11',
        # Как часто запускать DAG
        schedule_interval=timedelta(days=1),
        # С какой даты начать запускать DAG
        # Каждый DAG "видит" свою "дату запуска"
        # это когда он предположительно должен был
        # запуститься. Не всегда совпадает с датой на вашем компьютере
        start_date=datetime(2022, 4, 1),
        # Запустить за старые даты относительно сегодня
        # https://airflow.apache.org/docs/apache-airflow/stable/dag-run.html
        catchup=False,
        # теги, способ помечать даги
        tags=['HW_11', 'a.kovsharov']
) as dag:
    def get_var():
        from airflow.models import Variable
        print(Variable.get("is_startml"))

    t1 = PythonOperator(
        task_id='get_is_startml',
        python_callable=get_var
    )

    t1
