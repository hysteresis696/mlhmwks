import datetime
import psycopg2
from psycopg2.extras import RealDictCursor
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException, Depends

app = FastAPI()


# task 1
@app.get("/task1")
def print_hello():
    return {"message": "hello, world"}


# task 2
@app.get("/task2")
def two_sum(a: int, b: int) -> int:
    return a + b


# task 3
@app.get("/sum_date")
def sum_date(current_date: datetime.date, offset: int):
    valid = datetime.timedelta(days=offset)
    return current_date + valid


# task 4
class User(BaseModel):
    name: str
    surname: str
    age: int
    registration_date: datetime.date

    class Config:
        orm_mode = True


# task 5
@app.post("/user/validate")
def user_valid(json_file: User):
    return f"Will add user: {json_file.name} {json_file.surname} with age {json_file.age}"


# task 6
@app.get("/user/{id}")
def get_user(id: int):
    conn = psycopg2.connect(
        "postgresql://robot-startml-ro:pheiph0hahj1Vaif@postgres.lab.karpov.courses:6432/startml",
        cursor_factory=RealDictCursor
    )
    cursor = conn.cursor()
    cursor.execute(
        f"""
        SELECT gender, age, city
        FROM "user"
        WHERE id={id}
        """
    )
    return cursor.fetchall()


# task 7
@app.get("/userwcor/{id}")
def get_user(id: int):
    conn = psycopg2.connect(
        "postgresql://robot-startml-ro:pheiph0hahj1Vaif@postgres.lab.karpov.courses:6432/startml",
        cursor_factory=RealDictCursor
    )
    cursor = conn.cursor()
    cursor.execute(
        f"""
        SELECT gender, age, city
        FROM "user"
        WHERE id={id}
        """
    )
    result = cursor.fetchall()

    if not result:
        raise HTTPException(404, "user not found")
    else:
        return result


# task 8

def get_db():
    conn = psycopg2.connect(
        "postgresql://robot-startml-ro:pheiph0hahj1Vaif@postgres.lab.karpov.courses:6432/startml",
        cursor_factory=RealDictCursor
    )
    return conn


@app.get("/userdep/{id}")
def user_dep(id, db=Depends(get_db)):
    with db.cursor() as cursor:
        cursor.execute(
            f"""
            SELECT gender, age, city
            FROM "user"
            WHERE id={id}
            """
        )
        result = cursor.fetchall()

        if not result:
            raise HTTPException(404, "user not found")
        else:
            return result


# task 9
class PostResponse(BaseModel):
    id: int
    text: str
    topic: str

    class Config:
        orm_mode = True
@app.get("/post/{id}", response_model=PostResponse)
def anser_validate(id, db=Depends(get_db)) -> PostResponse:
    with db.cursor() as cursor:
        cursor.execute(
            f"""
             SELECT id, text, topic
             FROM post
             WHERE id = {id}
             """
        )

        result = cursor.fetchone()

        if not result:
            raise HTTPException(404, "post not found")
        else:
            return PostResponse(**result)

        

