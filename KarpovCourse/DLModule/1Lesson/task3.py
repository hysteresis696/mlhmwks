import torch
from task2 import function02

'''Напишите функцию function03. Она должна принимать тензор-матрицу с объектами и
 тензор-вектор с правильными ответами, будем решать задачу регрессии:
  def function03(x: torch.Tensor, y: torch.Tensor):

Создайте внутри функции веса для линейной регрессии (без свободного коэффициента),
можете воспользоваться функцией из предыдущего степа.
С помощью градиентного спуска подберите оптимальные веса для входных данных
(используйте длину шага около 1e-2). Верните тензор-вектор с оптимальными весами из функции.
Ваши обученные веса должны давать MSE на обучающей выборке меньше единицы.'''


def function03(x: torch.Tensor, y: torch.Tensor):
    step = 1e-2
    weights = function02(x)

    while True:
        predictions = torch.matmul(weights, x)

        mse = torch.sum((predictions - y) ** 2)

        mse.backward()

        with torch.no_grad():
            weights -= weights.grad * step

        weights.grad.zero_()

        return weights


test = torch.randn(2, 6)

result = function02(test, )
print(result)
