import torch
'''Напишите функцию function01. Она должна иметь следующую сигнатуру:

def function01(tensor: torch.Tensor, count_over: str) -> torch.Tensor:

Если count_over равен 'columns', верните среднее тензора по колонкам.
Если равен 'rows', то верните среднее по рядам. Гарантируется,
что тензор будет матрицей (то есть будет иметь размерность 2).'''


def function01(tensor: torch.Tensor, count_over: str) -> torch.Tensor():
    return torch.mean(tensor, dim=1) if count_over == 'rows' else torch.mean(tensor, dim=0)


test = torch.randn(2, 6)

result = function01(test, count_over='rows')
print(result)
