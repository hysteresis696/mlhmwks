import torch
import torch.nn as nn
from task2 import function02

'''Напишите функцию function04. Она должна принимать тензор-матрицу с объектами и тензор с правильными ответами,
 будем решать задачу регрессии: def function04(x: torch.Tensor, y: torch.Tensor):

Создайте внутри функции полносвязный слой, обучите этот полносвязный слой на входных данных с помощью
градиентного спуска (используйте длину шага около 1e-2). Верните его из функции.
Ваш полносвязный слой должен давать MSE на обучающей выборке меньше 0.3.'''


def function04(x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
    step = 1e-2
    weights = function02(x)

    in_features = x.shape[1]
    out_features = 1 if y.dim() == 1 else y.shape[0]

    layer = nn.Linear(in_features, out_features)
    while True:
        predictions = layer(x).ravel()

        mse = torch.mean((predictions - y) ** 2)

        mse.backward()

        with torch.no_grad():
            layer.weight -= step * layer.weight.grad
            layer.bias -= step * layer.bias.grad

        layer.weight.grad.zero_()
        layer.bias.grad.zero_()

        return weights


n_features = 2
n_objects = 300

w_true = torch.randn(n_features)
X = (torch.rand(n_objects, n_features) - 0.5) * 5
Y = X @ w_true + torch.randn(n_objects) / 2

print(function04(X, Y))
