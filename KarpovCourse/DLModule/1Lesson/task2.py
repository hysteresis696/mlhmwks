import torch
'''Напишите функцию function02. Функции на вход должен приходить датасет
— тензор-матрица признаков объектов. Ваша функция должна создать тензор-вектор
с весами (пусть они будут из равномерного распределения на отрезке от 0 до 1) и 
вернуть их для дальнейшего обучения линейной регрессии без свободного коэффициента.
Сделайте эти веса типа float32, для них нужно будет в процессе обучения вычислять градиенты 
(воспользуйтесь requires_grad).'''


def function02(dataset: torch.Tensor()) -> torch.Tensor:
    weights = torch.rand(dataset.shape[1], dtype=torch.float32, requires_grad=True)
    return weights


test = torch.randn(2, 6)

result = function02(test)
print(result)