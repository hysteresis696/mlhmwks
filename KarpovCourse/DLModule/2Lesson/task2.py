import torch.nn as nn
from torch.utils.data import DataLoader
from torch.optim import Optimizer


'''Напишите функцию train. Она должна принимать на вход нейронную сеть, даталоадер,
оптимизатор и функцию потерь. Она должна иметь следующую сигнатуру:
 def train(model: nn.Module, data_loader: DataLoader, optimizer: Optimizer, loss_fn):
Внутри функции сделайте следующие шаги:
Переведите модель в режим обучения.
Проитерируйтесь по даталоадеру.
На каждой итерации:
    Занулите градиенты с помощью оптимизатора
    Сделайте проход вперед (forward pass)
    Посчитайте ошибку
    Сделайте проход назад (backward pass)
    Напечатайте ошибку на текущем батче с точностью до 5 символов после запятой (только число)
    Сделайте шаг оптимизации

Функция должна вернуть среднюю ошибку за время прохода по даталоадеру.'''


def train(model: nn.Module, data_loader: DataLoader, optimizer: Optimizer, loss_fn):
    model.train()
    total_loss = 0

    for x, y in data_loader:
        optimizer.zero_grad()

        output = model(x)

        loss = loss_fn(output, y)
        total_loss += loss.item()

        loss.backward()

        print(f'{loss.item():.5f}')

        optimizer.step()

    return total_loss / len(data_loader)
