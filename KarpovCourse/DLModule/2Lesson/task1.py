import torch.nn as nn

'''Напишите функцию create_model, которая должна возвращать полносвязную нейронную сеть из двух слоев.
На вход должно быть 100 чисел, на выход 1, посередине 10. В качестве нелинейности используйте ReLU.
Воспользуйтесь nn.Sequential и передайте слои как последовательность.'''


def create_model() -> nn.Sequential:
    model = nn.Sequential(
        nn.Linear(100, 10),
        nn.ReLU(),
        nn.Linear(10, 1),
        )
    return model


model = create_model()
print((list(model.parameters())))
