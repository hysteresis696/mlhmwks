import torch
import torch.nn as nn
from torch.utils.data import DataLoader


"""Напишите функцию evaluate. Она должна принимать на вход нейронную сеть,
 даталоадер и функцию потерь. Она должна иметь следующую сигнатуру:
  def evaluate(model: nn.Module, data_loader: DataLoader, loss_fn):

Внутри функции сделайте следующие шаги:
    Переведите модель в режим инференса (применения)
    Проитерируйтесь по даталоадеру
    На каждой итерации:
    Сделайте проход вперед (forward pass)
    Посчитайте ошибку

Функция должна вернуть среднюю ошибку за время прохода по даталоадеру."""


@torch.inference_mode()
def evaluate(model: nn.Module, data_loader: DataLoader, loss_fn):
    model.eval()
    total_loss = 0

    for x, y in data_loader:
        output = model(x)
        loss = loss_fn(output, y)
        total_loss += loss.item()

    return total_loss / len(data_loader)
